#!/bin/bash

echo -e "test 1: "
curl http://localhost:8082/message -X POST -d '{"messageId":1, "timestamp":1234, "protocolVersion":"1.0.0", "messageData":{"mMX":1234, "mPermGen":12345}}'
echo ""
echo -e "test 2: "
curl http://localhost:8082/message -X POST -d '{"messageId":2, "timestamp":2234, "protocolVersion":"1.0.1", "messageData":{"mMX":1234, "mPermGen":56783, "mOldGen":22222}}'
echo ""
echo -e "test 3: "
curl http://localhost:8082/message -X POST -d '{"messageId":3, "timestamp":3234, "protocolVersion":"2.0.0", "payload":{"mMX":1234, "mPermGen":5678, "mOldGen":22222, "mYoungGen":333333}'
echo ""
