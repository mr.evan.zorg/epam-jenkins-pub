# Jenkins project

## Requirements

 - Linux with docker
 - Git repository
 - Artifactory server
 - SonarQube server

## Description

Project:
 - Configures docker to work on ports
 - Deploys a Jenkins container with plugins on docker
 - Work with code
   - Assembles a project
   - Uploads artifacts to the Artifactory server
 - Preparation of the test environment
   - Deploy Rabbit container
   - Deploy Java container with artefact message-processor
   - Deploy Tomcat container with artefact message-gateway
 - running tests

## Run

### Configures docker to work on ports
Do not forget change variables!
```
./docker_configure.sh
```

### Get plugin list from Jenkins
Java needed
```
./get_jenkins_plugin_list.sh
```

### Manual start conteiners Rabbit, Java, Tomcat with artifacts (optional)
```
./containers_install.sh
```

### Manual start tests of application
```
./test_app.sh
```

### Get the docker images
```
https://cloud.docker.com/repository/docker/victorvk/rabbitmq
https://cloud.docker.com/repository/docker/victorvk/java8-kabanov
https://cloud.docker.com/repository/docker/victorvk/tomcat8-kabanov

docker push victorvk/rabbitmq:lastest
docker push victorvk/java8_kabanov:lastest
docker push victorvk/tomcat8_kabanov:lastest
```

## Application message formats
Message can be POSTed in 3 different json formats, structured as follows:

_message v1.0.0_
```
{"messageId":1, "timestamp":1234, "protocolVersion":"1.0.0", "messageData":{"mMX":1234, "mPermGen":1234}}
```

_message v1.0.1_
```
{"messageId":2, "timestamp":2234, "protocolVersion":"1.0.1", "messageData":{"mMX":1234, "mPermGen":5678, "mOldGen":22222}}
```

_message v2.0.0_
```
{"messageId":3, "timestamp":3234, "protocolVersion":"2.0.0", "payload":{"mMX":1234, "mPermGen":5678, "mOldGen":22222, "mYoungGen":333333}}
```

