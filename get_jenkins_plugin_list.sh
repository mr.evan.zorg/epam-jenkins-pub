#!/bin/sh
# в контейнерах может не быть bash
cd /tmp
curl 'localhost:8080/jnlpJars/jenkins-cli.jar' > jenkins-cli.jar
echo 'def plugins = jenkins.model.Jenkins.instance.getPluginManager().getPlugins()' > plugins.groovy
echo 'plugins.each {println "${it.getShortName()}: ${it.getVersion()}"}' >> plugins.groovy
java -jar jenkins-cli.jar -s http://localhost:8080 -auth admin:pass groovy = < plugins.groovy > /tmp/plugin.txt
