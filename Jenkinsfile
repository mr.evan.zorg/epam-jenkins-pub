timestamps {
    node('master') {
        withDockerServer([uri: '192.168.0.75:2376', credentialsId: 'DOCKER']) {
            stage('Git clone') {
                dir('gitDir') {
                    echo "======================== GIT FETCH START =========================="
                    deleteDir()
                    git branch: 'feature/kabanov',
                            credentialsId: 'gitlab-user-account',
                            url: 'git@gitlab.com:devops7spring/jenkins.git'
                    echo "======================== GIT FETCH FINISH =========================="

                    echo "======================== BUILD START =========================="
                    def server = Artifactory.server 'artefact-1'
                    def rtMaven = Artifactory.newMavenBuild()
                    rtMaven.resolver server: server, releaseRepo: 'libs-release', snapshotRepo: 'libs-snapshot'
                    rtMaven.deployer server: server, releaseRepo: 'libs-release-local', snapshotRepo: 'libs-snapshot-local'
                    rtMaven.deployer.deployArtifacts = false
                    rtMaven.tool = 'maven3'
                    def buildInfo = rtMaven.run pom: 'pom.xml', goals: 'clean package'
                    println("BUILD INFO SIZE: ${buildInfo.getArtifacts().size()}")
                    rtMaven.deployer.deployArtifacts buildInfo
                    server.publishBuildInfo buildInfo
                    println("BUILD INFO SIZE: ${buildInfo.getArtifacts().size()}")

                    archiveArtifacts artifacts: 'message-processor/target/message-processor-1.0-SNAPSHOT.jar'
                    archiveArtifacts artifacts: 'message-gateway/target/message-gateway-1.0-SNAPSHOT.war'

                    def JAR = "http://192.168.0.75:8081/artifactory/libs-snapshot-local/message-processor/message-processor/1.0-SNAPSHOT/message-processor-1.0-20190918.121442-1.jar"
                    def WAR="http://192.168.0.75:8081/artifactory/libs-snapshot-local/messaging-gateway/message-gateway/1.0-SNAPSHOT/message-gateway-1.0-20190918.121442-1.war"

                    sh "./mysed.sh %JAR% $JAR Dockerfile-openjdk8"
                    sh "./mysed.sh %WAR% $WAR Dockerfile-tomcat8"

                    echo "======================== BUILD FINISH =========================="

                    withEnv(["PATH+DOCKER=${tool 'dockercli'}/bin"]) {
                        stage('Docker deploy') {
                            try {
                                sh 'docker rm -f message-processor'
                            } catch (Exception e) {
                                echo "No message-processor conteiner found!"
                                currentBuild.result = 'SUCCESS'
                            }
                            try {
                                sh 'docker rm -f message-gateway'
                            } catch (Exception e) {
                                echo "No message-gateway conteiner found!"
                                currentBuild.result = 'SUCCESS'
                            }
                            try {
                                sh 'docker rm -f rabbit'
                            } catch (Exception e) {
                                echo "No rabbit conteiner found!"
                                currentBuild.result = 'SUCCESS'
                            }

                            withCredentials([string(credentialsId: 'hub.docker.com', variable: 'HUB_DOCKER')]) {
                                sh 'docker login --username=victorvk --password=\${HUB_DOCKER}'
                            }
                            sh 'docker run -d --hostname rabbit --name rabbit -p 5672:5672 -p 15672:15672 rabbitmq:3-management'
                            sh 'docker tag $(docker images -q rabbitmq:3-management) victorvk/rabbitmq:lastest'
                            sh 'docker push victorvk/rabbitmq:lastest'
                            sh 'docker build -t java8-kabanov:lastest -f Dockerfile-openjdk8 .'
                            sh 'docker tag $(docker images -q java8-kabanov:lastest) victorvk/java8-kabanov:lastest'
                            sh 'docker push victorvk/java8-kabanov:lastest'
                            sh 'docker run --rm -d --name message-processor java8-kabanov:lastest'
                            sh 'docker build -t tomcat8-kabanov:lastest -f Dockerfile-tomcat8 .'
                            sh 'docker run --rm -d --name message-gateway -p 8082:8080 tomcat8-kabanov:lastest'
                            sh 'docker tag $(docker images -q tomcat8-kabanov:lastest) victorvk/tomcat8-kabanov:lastest'
                            sh 'docker push victorvk/tomcat8-kabanov:lastest'
                            sh 'docker ps'
                        }
                        stage('Tests of app') {
                            dir('gitDir') {
                                echo "======================== TESTS START =========================="

                                def message1 = "12345"
                                sh "curl http://192.168.0.75:8082/message -X POST -d \'{\"messageId\":1, \"timestamp\":1234, \"protocolVersion\":\"1.0.0\", \"messageData\":{\"mMX\":1234, \"mPermGen\":${message1}}}\'"
                                result = sh(script: "docker logs message-processor | grep \"${message1}\"", returnStdout: true).trim()

                                if (result != '') {
                                    println("Test [1] is OK");
                                } else {
                                    currentBuild.result = 'FAILURE'
                                }

                                def message2 = "54321"
                                sh "curl http://192.168.0.75:8082/message -X POST -d \'{\"messageId\":2, \"timestamp\":2234, \"protocolVersion\":\"1.0.1\", \"messageData\":{\"mMX\":1234, \"mPermGen\":${message2}, \"mOldGen\":22222}}\'"
                                result = sh(script: "docker logs message-processor | grep \"${message2}\"", returnStdout: true).trim()

                                if (result != '') {
                                    println("Test [2] is OK");
                                } else {
                                    currentBuild.result = 'FAILURE'
                                }

                                def message3 = "94529"
                                sh "curl http://192.168.0.75:8082/message -X POST -d \'{\"messageId\":3, \"timestamp\":5534, \"protocolVersion\":\"2.0.0\", \"payload\":{\"mMX\":1234, \"mPermGen\":${message3}, \"mOldGen\":22222, \"mYoungGen\":333333}}\'"
                                result = sh(script: "docker logs message-processor | grep \"${message3}\"", returnStdout: true).trim()

                                if (result != '') {
                                    println("Test [3] is OK");
                                } else {
                                    currentBuild.result = 'FAILURE'
                                }

                                echo "======================== TESTS FINISH =========================="
                            }
                        }

                    }

                    stage('SonarQube tests') {
                        withEnv(["PATH+MAVEN=${tool 'maven3'}/bin"]) {
                            withCredentials([string(credentialsId: 'sonar', variable: 'SONAR_TOKEN')]) {
                                echo "======================== SonarQube tests START =========================="
                                sh 'mvn clean package sonar:sonar -Dsonar.login=\${SONAR_TOKEN}'
                                echo "======================== SonarQube tests FINISH =========================="
                            }
                        }
                    }
                }
            }
        }
    }
}
