#!/bin/bash
 
### центр сертификации ###
# fqdn сервера с docker-ом, по этому имени мы будем подключаться. Нет, ip адрес не подходит.
export HOST="vagrant.memo-spb.ru"
SSL_C="RU"
SSL_ST="Saint-Petersburg"
SSL_L="Saint-Petersburg"
SSL_O="Global Security"
SSL_OU="IT Department"
SSL_CN=$HOST
 
# директория для центра сертификации
mkdir -p cc
# фраза-пароль на закрытый ключ
openssl rand -base64 32 > cc/passfile.txt
# закрытый ключ
openssl genrsa -aes256 -out cc/ca-key.pem -passout file:cc/passfile.txt 4096
# открытый ключ (неинтерактивно)
openssl req -new -x509 -days 365 -key cc/ca-key.pem -sha256 -out cc/ca.pem -passin file:cc/passfile.txt -subj "/C=$SSL_C/ST=$SSL_ST/L=$SSL_L/O=$SSL_O/OU=$SSL_OU/CN=$HOST"
 
### Сервер ###
# директория для сервера
mkdir -p server
# ключ сервера
openssl genrsa -out server/server-key.pem 4096
# запрос подписи сертификата
openssl req -subj "/CN=$HOST" -sha256 -new -key server/server-key.pem -out server/server.csr
# файл атрибутов
# - ip адреса сервера подключение на которые будет валидно
# - ключ только для аутентификации сервера
echo subjectAltName = DNS:$HOST,IP:192.168.0.75,IP:127.0.0.1 > server/extfile.cnf
echo extendedKeyUsage = serverAuth >> server/extfile.cnf
# сгенерить подписанный сертификат
openssl x509 -req -days 365 -sha256 -in server/server.csr -CA cc/ca.pem -CAkey cc/ca-key.pem -CAcreateserial -out server/server-cert.pem -extfile server/extfile.cnf -passin file:cc/passfile.txt
 
### Клиент ###
# директория для клиента
mkdir -p client
# клиенсткий ключ
openssl genrsa -out client/key.pem 4096
# запрос на подпись
openssl req -subj '/CN=client' -new -key client/key.pem -out client/client.csr
# файл конфигурации расширений клиента
echo extendedKeyUsage = clientAuth > client/extfile-client.cnf
# сгенерить подписанный сертификат
openssl x509 -req -days 365 -sha256 -in client/client.csr -CA cc/ca.pem -CAkey cc/ca-key.pem -CAcreateserial -out client/cert.pem -extfile client/extfile-client.cnf -passin file:cc/passfile.txt
 
# подравить права доступа
chmod -v 0400 cc/ca-key.pem client/key.pem server/server-key.pem
chmod -v 0444 cc/ca.pem server/server-cert.pem client/cert.pem
# удалить лишнее
rm -v client/client.csr server/server.csr client/extfile-client.cnf server/extfile.cnf

