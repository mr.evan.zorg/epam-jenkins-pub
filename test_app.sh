#!/bin/bash

message=$1

id=$(awk -v min=1 -v max=100 'BEGIN{srand(); print int(min+rand()*(max-min+1))}')
timestamp=$(date "+%s")
echo -e "message sent (proto 1.0.0): $(curl http://localhost:8082/message -X POST -d '{"messageId":'$id', "timestamp":'$timestamp', "protocolVersion":"1.0.0", "messageData":{"mMX":1234, "mPermGen":'$message'}}' 2>/dev/null)"
docker logs message-processor | grep $id | grep $timestamp | grep $message 1>2 2>/dev/null
if [ $? -eq 0 ]
then
  echo 'message-processor say OK'
else
  echo 'message-processor say ERROR'
fi
sleep 1

id=`awk -v min=1 -v max=100 'BEGIN{srand(); print int(min+rand()*(max-min+1))}'`
timestamp=`date "+%s"`
echo -e "message sent (proto 1.0.1): $(curl http://localhost:8082/message -X POST -d '{"messageId":'$id', "timestamp":'$timestamp', "protocolVersion":"1.0.1", "messageData":{"mMX":1234, "mPermGen":'$message', "mOldGen":22222}}' 2>/dev/null)"
docker logs message-processor | grep $id | grep $timestamp | grep $message 1>2 2>/dev/null
if [ $? -eq 0 ]
then
  echo 'message-processor say OK'
else
  echo 'message-processor say ERROR'
fi
sleep 1

id=`awk -v min=1 -v max=100 'BEGIN{srand(); print int(min+rand()*(max-min+1))}'`
timestamp=`date "+%s"`
echo -e "message sent (proto 2.0.0): $(curl http://localhost:8082/message -X POST -d '{"messageId":'$id', "timestamp":'$timestamp', "protocolVersion":"2.0.0", "payload":{"mMX":1234, "mPermGen":'$message', "mOldGen":22222, "mYoungGen":333333}}' 2>/dev/null)"
docker logs message-processor | grep $id | grep $timestamp | grep $message 1>2 2>/dev/null
if [ $? -eq 0 ]
then
  echo 'message-processor say OK'
else
  echo 'message-processor say ERROR'
fi

