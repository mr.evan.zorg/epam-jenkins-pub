#!/bin/bash

docker rm -f message-processor
docker rm -f message-gateway

docker build -t java8_kabanov:1 -f Dockerfile-openjdk8 .
docker run --rm -d --name message-processor java8_kabanov:1
docker build -t tomcat8_kabanov:1 -f Dockerfile-tomcat8 .
docker run --rm -d --name message-gateway -p 8082:8080 tomcat8_kabanov:1
